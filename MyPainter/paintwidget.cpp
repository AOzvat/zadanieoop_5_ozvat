#include "paintwidget.h"

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 50;
	myPenColor = Qt::red;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::SaltPepperImage(){
	QRgb black = 0;
	QRgb white = 16777215;

	//nahodny vyber pixelov
	for (int i = 0; i < ((image.height() * image.width()) / 10); i++) {
		int row = rand() % image.width();
		int column = rand() % image.height();
		int rand_color = rand() % 2;

		//zmena vybraneho pixelu na ciernu alebo bielu
		if (rand_color == 1) {
			image.setPixelColor(row, column, black);
		}
		else {
			image.setPixelColor(row, column, white);
		}
	}

	update();
}

void PaintWidget::MedianImage(){
	int r, g, b;
	QList<int> list_r, list_g, list_b;

	//prejde cely obrazok
	for (int i = 1; i < image.width() - 1; i++) {
		for (int j = 1; j < image.height() - 1; j++) {

			//z matice 3x3 vyberie farby pixelov
			for (int k = i - 1; k <= i + 1; k++) {
				for (int l = j - 1; l <= j + 1; l++) {
					QRgb tempRgb = image.pixel(QPoint(k, l));
					QColor tempColor(tempRgb);

					//ziskame zlozke pixelu
					r = tempColor.red();
					g = tempColor.green();
					b = tempColor.blue();

					list_r.append(r);
					list_g.append(g);
					list_b.append(b);
				}
			}

			//vysortuje list
			qSort(list_r);
			qSort(list_g);
			qSort(list_b);

			//nastavy pixel na obrazku na hodnotu 4. prvku list (prostedny pixel v matici)
			image.setPixelColor(i, j, qRgb(list_r[4], list_g[4], list_b[4]));

			//vycisti list
			list_r.clear();
			list_g.clear();
			list_b.clear();
		}
	}

	update();
}


void PaintWidget::SepiaImage(){
	for (int i = 0; i < image.width(); i++) {
		for (int j = 0; j < image.height(); j++) {
			QRgb tempRgb = image.pixel(QPoint(i, j));
			QColor tempColor(tempRgb);

			//ziskame zlozke pixelu
			int r = tempColor.red();
			int g = tempColor.green();
			int b = tempColor.blue();
			
			//preratame na sepiovu farbu
			int tr = (int)(0.393*r + 0.769*g + 0.189*b);
			int tg = (int)(0.349*r + 0.686*g + 0.168*b);
			int tb = (int)(0.272*r + 0.534*g + 0.131*b);

			//par podmienok :D
			if (tr > 255) {
				r = 255;
			}
			else {
				r = tr;
			}

			if (tg > 255) {
				g = 255;
			}
			else {
				g = tg;
			}

			if (tb > 255) {
				b = 255;
			}
			else {
				b = tb;
			}

			image.setPixel(i, j, qRgb(r, g, b));
		}
	}

	update();
}
